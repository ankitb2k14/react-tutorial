var React = require('react');
var CreateClass = require('create-react-class');

require('./css/addItem.css');

var AddItem = CreateClass({
    render: function(){
        return (
            <form id="add-todo" onSubmit={this.handleSubmit}>
                <input type="text" required ref="addItem"/>
                <input type="submit" value="Hit Me" />
            </form>
        )
    },

    //Custom Functions
    handleSubmit: function(e){
        e.preventDefault();
        this.props.onAdd(this.refs.addItem.value);
    }
});

module.exports = AddItem;