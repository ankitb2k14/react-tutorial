var React = require('react');
var CreateClass = require('create-react-class');

require('./css/todoItem.css')
var ToDoItem = CreateClass({
    render: function(){
        return (
            <li>
                <div className="todo-item">
                    <span className="item-name">{this.props.item}</span>
                    <span className="delete-item" onClick={this.handleDelete}> x</span>
                </div>
            </li>
        )
    },

    //Custom Functions
    handleDelete: function(){
        this.props.onDelete(this.props.item);
    }
})

module.exports = ToDoItem;