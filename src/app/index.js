var React = require('react');
var ReactDOM = require('react-dom');
var CreateClass = require('create-react-class');

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

require('./css/index.css');
//Module

var ToDoItem = require('./todoItem');
var AddItem = require('./addItem');
var AboutComponent = require('./about'); 
//Create a component

var AppRouter = CreateClass({
    render: function(){
        return(
            <Router>
                <div> 
                    <Route exact path='/' component={ToDoComponent}></Route>
                    <Route path='/about' component={AboutComponent}></Route>
                </div>
            </Router>
        )
    }
})


var ToDoComponent = CreateClass({
    getInitialState: function(){
        return {
            todos: ['wash up', 'eat cheese', 'sleep', 'refresh'],
            age: 30
        }
    },//getInitialState
    render: function(){
        var todos = this.state.todos;
        todos = todos.map(function(item, index){
            return(
                <ToDoItem item={item} key={index} onDelete={this.onDelete}/>
            )
        }.bind(this));
        return( 
            <div id="todo-list">
                <Link to='/about'>About</Link>
                <p onClick={this.clicked}>Example of click</p>
                <ul>
                    {todos}
                </ul>
                <AddItem onAdd={this.onAdd}/>
            </div>
        )
    },//render

    //Custom Functions
    onDelete: function(item){
        var updatedTodos = this.state.todos.filter(function(value, index){
            return item !== value;
        });
        this.setState({
            todos: updatedTodos
        })
    },
    onAdd: function(item){
        var updatedTodos = this.state.todos;
        updatedTodos.push(item);
        this.setState({
            todos: updatedTodos
        })
    },

    //lifecycle 

    componentWillMount: function(){
        console.log('ComponentWillMount');
    },
    componentDidMount: function(){
        console.log('ComponentDidMount');
    },
    componentWillUpdate: function(){
        console.log('ComponentWillUpdate');
    },
    componentDidUpdate: function(){
        console.log('componentDidUpdate');
    }
});

ReactDOM.render(<AppRouter />, document.getElementById('todo-wrapper'));