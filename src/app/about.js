var React = require('react');
var CreateClass = require('create-react-class');
import { Link } from 'react-router-dom';


var AboutComponent = CreateClass({
    render: function(){
        return(
            <div>
                <Link to='/'>Home</Link>
                <p>About</p>
            </div>
        )
    }
})

module.exports = AboutComponent;